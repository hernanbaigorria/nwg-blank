<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Titulo de la pagina
	$ADDED_CFG['page_title']['default']			 					= 'Iarse';
	$ADDED_CFG['page_title']['multilang'] 	 						= TRUE;
	$ADDED_CFG['page_title']['permissions'] 						= FALSE;
	$ADDED_CFG['page_title']['type'] 								= 'text';

	// Descripcion de la pagina
	$ADDED_CFG['body_html']['default'] 								= '';
	$ADDED_CFG['body_html']['multilang'] 							= FALSE;
	$ADDED_CFG['body_html']['permissions'] 							= FALSE;
	$ADDED_CFG['body_html']['type'] 								= 'textarea';

