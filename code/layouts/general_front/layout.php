<!DOCTYPE html>
<html dir="ltr" lang="es">
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="description" content=" " />
        <meta name="keywords" content=" " />

        <title>IARSE</title>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_HEADER ?>
            <?php echo $EDITOR_FOOTER ?>
        <?php endif ?>

        <!-- Favicon and Touch Icons -->
        <link href="<?php echo THEME_ASSETS_URL ?>general/images/favicon.ico" rel="shortcut icon">
        
        <!-- Stylesheet -->

        <link href="<?php echo THEME_ASSETS_URL ?>general/css/jquery-ui.min.css" rel="stylesheet" type="text/css">


        <link href="<?php echo THEME_ASSETS_URL ?>general/css/animate.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/css-plugin-collections.css" rel="stylesheet"/>
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet" id="menuzord-menu-skins"/>
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/style-main.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/preloader.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/colors/theme-skin-color-set-1.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/bootstrap.min.css" rel="stylesheet" type="text/css">


        
        <?php if (!$EDITABLE): ?>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/jquery-ui.min.js"></script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/bootstrap.min.js"></script>
        <?php endif ?>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>

        <script src="<?php echo THEME_ASSETS_URL ?>general/js/jquery-plugin-collection.js"></script>
        
        
        <!-- JS | jquery plugin collection for this theme -->
        <!-- Revolution Slider 5.x SCRIPTS -->
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            <?php echo $PAGE['page_custom_css'] ?>
        </style>
        
    </head>
    <body class="">
        <div id="wrapper" class="clearfix">
            <!-- preloader -->
            <div id="preloader">
                <div id="spinner">
                    <div class="preloader-dot-loading">
                        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
                    </div>
                </div>
            </div>
            <!-- Header -->
            <header id="header" class="header">
                <div class="header-top header-color sm-text-center p-0">
                    <div class="container">
                        <div class="row flex-header-info">
                            <div class="col-md-4">
                                <div class="widget no-border m-0">
                                    <ul class="list-inline font-13 sm-text-center mt-5">
                                        <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                                        <li>
                                            <a class="text-white ft-size" href="#"><i class="fa fa-phone text-white mg-rg-5"></i><?php echo $PAGE_CFG['general_phone'] ?></a>
                                        </li>
                                        <?php endif ?>
                                        <li class="text-white"></li>
                                        <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                                        <li>
                                            <a class="text-white ft-size" href="mailto:<?php echo $PAGE_CFG['general_mail'] ?>"><i class="fa fa-envelope text-white mg-rg-5"></i><?php echo $PAGE_CFG['general_mail'] ?></a>
                                        </li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                           
                            <div class="col-md-8 mg-tp-5 flex-col-redes">                
                                <div class="widget redes-custom no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
                                    <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                                        <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                                        <li><a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank"><i class="fa fa-facebook ft-size-20 text-white"></i></a></li>
                                        <?php endif ?>
                                        <?php if (!empty($PAGE_CFG['page_social_twitter'])): ?>
                                        <li><a href="<?php echo page_uri($PAGE_CFG['page_social_twitter']) ?>" target="_blank"><i class="fa fa-twitter ft-size-20 text-white"></i></a></li>
                                        <?php endif ?>
                                        <?php if (!empty($PAGE_CFG['page_social_youtube'])): ?>
                                        <li><a href="<?php echo page_uri($PAGE_CFG['page_social_youtube']) ?>" target="_blank"><i class="fa fa-youtube ft-size-20 text-white"></i></a></li>
                                        <?php endif ?>
                                        <?php if (!empty($PAGE_CFG['page_social_instagram'])): ?>
                                        <li><a href="<?php echo page_uri($PAGE_CFG['page_social_instagram']) ?>" target="_blank"><i class="fa fa-instagram ft-size-20 text-white"></i></a></li>
                                        <?php endif ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-nav">
                    <div class="header-nav-wrapper navbar-scrolltofixed color-nav border-bottom-theme-color-2-1px">
                        <div class="">
                            <nav id="menuzord" class="menuzord flex-nav color-nav pull-left flip menuzord-responsive">
                                <a href="/" class="margin-auto">
                                <img src="<?php echo THEME_ASSETS_URL ?>general/images/logo.png" alt="" class="hidden-lg hidden-sm hidden-md cust-lgo-res">
                                </a>
                                <ul class="menuzord-menu">
                                    <?php // MENU 1 ?>
                                    <?php 
                                        for ($i=1; $i <= 4; $i++) { 
                                        ?>
                                        <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                                        <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                                            <li class="<?php if ($ID_PAGE == $PAGE_CFG['page_main_menu_link'.$i] OR in_array($ID_PAGE, $gtp_pages)) echo 'active' ?>">
                                                <?php 
                                                    $this_url = page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE); 
                                                    $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                ?>
                                                <a target="<?php echo $target ?>" href="<?php echo $this_url ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                                                <?php if (count($gtp_pages) >= 1 AND !empty($gtp_pages[0])): ?>
                                                
                                                        <ul class="dropdown">
                                                            <?php foreach ($gtp_pages as $key => $gtp_page): ?>
                                                                <li class="<?php if ($ID_PAGE == $gtp_page) echo 'active' ?>">
                                                                    <?php 
                                                                        $this_url = page_uri($gtp_page, $EDITABLE); 
                                                                        $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                                    ?>
                                                                    <a target="<?php echo $target ?>" href="<?php echo page_uri($gtp_page, $EDITABLE) ?>"><?php echo page_title($gtp_page, $LANG) ?></a>
                                                                </li><br>
                                                            <?php endforeach ?>
                                                        </ul>
                                                   
                                                <?php endif ?>
                                            </li> 
                                        <?php endif ?>
                                        <?php 
                                        }
                                    ?>
                                    <li>
                                        <a href="/" class="visible-sm visible-md visible-lg"><img src="<?php echo THEME_ASSETS_URL ?>general/images/logo.png" alt=""></a>
                                    </li>
                                    <?php 
                                        for ($i=5; $i <= 6; $i++) { 
                                        ?>
                                        <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                                        <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                                            <li class="<?php if ($ID_PAGE == $PAGE_CFG['page_main_menu_link'.$i] OR in_array($ID_PAGE, $gtp_pages)) echo 'active' ?>">
                                                <?php 
                                                    $this_url = page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE); 
                                                    $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                ?>
                                                <a target="<?php echo $target ?>" href="<?php echo $this_url ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                                                <?php if (count($gtp_pages) >= 1 AND !empty($gtp_pages[0])): ?>
                                                    <div class="sub-menu">
                                                        <ul>
                                                            <?php foreach ($gtp_pages as $key => $gtp_page): ?>
                                                                <li class="<?php if ($ID_PAGE == $gtp_page) echo 'active' ?>">
                                                                    <?php 
                                                                        $this_url = page_uri($gtp_page, $EDITABLE); 
                                                                        $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                                    ?>
                                                                    <a target="<?php echo $target ?>" href="<?php echo page_uri($gtp_page, $EDITABLE) ?>"><?php echo page_title($gtp_page, $LANG) ?></a>
                                                                </li><br>
                                                            <?php endforeach ?>
                                                        </ul>
                                                    </div>
                                                <?php endif ?>
                                            </li> 
                                        <?php endif ?>
                                        <?php 
                                        }
                                    ?>
                                </ul>
                                <ul class="pull-right flip hidden-sm hidden-xs">
                                    <li>
                                        <!-- Modal: Book Now Starts -->

                                        <a class="btn btn-miembro btn-flat color-bottom text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15" href="/sumarse">SUMATE COMO MIEMBRO</a>
                                        <!-- Modal: Book Now End -->
                                    </li>
                                </ul>
                                <div id="top-search-bar" class="collapse">
                                    <div class="container">
                                        <form role="search" action="#" class="search_form_top" method="get">
                                            <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                                            <span class="search-close"><i class="fa fa-search"></i></span>
                                        </form>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Start main-content -->
            <div class="page_components_containers" style="margin-top: 160px;">
                <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
            </div>

            <?php echo @$MODULE_BODY; ?>
            <!-- end main-content -->

            <!-- Footer -->
            <footer id="footer" class="footer padd layer-overlay overlay-dark-9" data-bg-img="http://placehold.it/1920x1280">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="widget dark">
                                <img class="mt-5 mb-20 max-logo-foot" alt="" src="<?php echo THEME_ASSETS_URL ?>general/images/logo.png">
                                <ul class="list-inline mt-5">
                                    <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                                    <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a class="text-gray"><?php echo $PAGE_CFG['general_phone'] ?></a> </li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['general_address'])): ?>
                                    <li class="m-0 pl-10 pr-10 flex-adress"> <i class="fa fa-map-marker text-theme-color-2 mr-5"></i> <a class="text-gray"><?php echo $PAGE_CFG['general_address'] ?></a> </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="widget dark">
                                <h4 class="widget-title">Accesos rápidos</h4>
                                <ul class="list angle-double-right list-border">
                                    <?php $menu = explode(',' , $PAGE_CFG['page_foot_menu_p1']) ?>
                                    <?php foreach ($menu as $_kpages=> $_id_page): ?>
                                        <li class="">
                                            <a href="<?php echo page_uri($_id_page, $EDITABLE) ?>"><?php echo page_title($_id_page) ?> </a>
                                        </li>
                                    <?php endforeach ?>             
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-1"></div>
                        <div class="col-sm-6 col-md-5">
                            <div class="widget dark">
                                <h5 class="widget-title mb-10">Nuestras redes</h5>
                                <ul class="styled-icons-foot icon-bordered icon-sm">
                                    <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                                    <li><a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank"><i class="fa fa-facebook text-white"></i></a></li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['page_social_twitter'])): ?>
                                    <li><a href="<?php echo page_uri($PAGE_CFG['page_social_twitter']) ?>" target="_blank"><i class="fa fa-twitter text-white"></i></a></li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['page_social_youtube'])): ?>
                                    <li><a href="<?php echo page_uri($PAGE_CFG['page_social_youtube']) ?>" target="_blank"><i class="fa fa-youtube text-white"></i></a></li>
                                    <?php endif ?>
                                    <?php if (!empty($PAGE_CFG['page_social_instagram'])): ?>
                                    <li><a href="<?php echo page_uri($PAGE_CFG['page_social_instagram']) ?>" target="_blank"><i class="fa fa-instagram text-white"></i></a></li>
                                    <?php endif ?>
                                </ul>
                            </div>
                            <div class="widget dark">
                                <h5 class="widget-title mb-10">Recibí nuestras novedades</h5>
                                <form class="frm-suscribe newsletter-form">
                                    <div class="input-group">
                                        <input required type="email" name="email" placeholder="Tu Email" class="form-control input-lg font-16">
                                        <span class="input-group-btn">
                                            <button class="btn color-btn-foot text-white btn-xs m-0 font-14" type="submit">
                                                <i class="fa fa-envelope-o text-white mg-rg-5" aria-hidden="true"></i>
                                                Enviar
                                            </button>
                                        </span>
                                    </div>
                                    <p class="lbl-suscripto hidden">Gracias, ¡Estaremos en contacto!</p>
                                    <p class="lbl-error-suscripto hidden">Ocurrió un error. Intente nuevamente.</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
        </div>

        <script type="text/javascript">
            <?php echo $PAGE['page_custom_js'] ?>
        </script>


        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/custom.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/jquery-custom.js"></script>

        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
    
    </body>
</html>